#!/usr/bin/env bash
PURPOSE="wrapper script for building current project"

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptBaseName=$(basename $scriptName)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables


# -- auto: common helper functions
isContainer() { grep -qE "/(docker|lxc)/" /proc/1/cgroup 2>/dev/null; }
printEnvs()   { local ENV_EXCEPTIONS="(^LS_|^XDG_|^SSH_|=;)"; env|grep -vE "${ENV_EXCEPTIONS}"|sort; }
printTitle()  { echo;echo; local msg="${*}"; linesno=$(( ${#msg} + 4 )); msglines=$(for i in `seq ${linesno}`;do echo -n "=";done);echo -e "${msglines}\n# ${msg} #\n${msglines}"; }
DEBUG() { local msg="$(date +%F_%T) [${scriptBaseName}] DEBUG ${*}"; [[ "true" = "${VERBOSE}" ]] && echo -e "${msg}" || true; }
PRINT() { local msg="$(date +%F_%T) [${scriptBaseName}] INFO  ${*}"; echo -e "${msg}"; }
ERROR() { local msg="$(date +%F_%T) [${scriptBaseName}] ERROR ${*}"; echo -e "${msg}"; exit ${RC:-1}; }
installOsPackages(){
  local OS_PACKAGES="${*}"
    if apk --version >/dev/null 2>/dev/null;then      PRINT "using apk package manager";  apk update && apk add              ${OS_PACKAGES};
  elif apt --version >/dev/null 2>/dev/null;then      PRINT "using apt package manager";  apt-get update -qq && apt install -y       ${OS_PACKAGES};
  elif dnf --version >/dev/null 2>/dev/null;then      PRINT "using dnf package manager";  dnf install -y       ${OS_PACKAGES};
  elif yum --version >/dev/null 2>/dev/null;then      PRINT "using yum package manager";  yum install -y       ${OS_PACKAGES};
  elif microdnf -h   >/dev/null 2>/dev/null;then      PRINT "using µdnf package manager"; microdnf install -y  ${OS_PACKAGES};
  else ERROR "no supported package manager found";fi
}
# -- /auto: common helper functions

# -- SCRIPT DEFAULTS OPTIONS
OUT_PATH="${scriptFullDir}/out"
RELEASE_TAG="latest"
# -- /SCRIPT DEFAULTS OPTIONS

# -- script usage
USAGE() {
  printTitle "USAGE"
  echo "PURPOSE: ${PURPOSE}"
  echo "SYNTAX:  ${scriptName} [OPTIONS] <SYS_NAME>"
  printTitle "SYS_NAME (SUPPORTED TARGET OS)"
  # echo "ALP3X                     Alpine Linux 3.x systems"
  # echo "LTS18                     Ubuntu LTS 18.04 systems"
  echo "RHEL7                     EL7 compatible systems"
  # echo "RHEL8                     EL8 compatible systems"
  printTitle "OPTIONS"
  echo "-h|--help                 print out this help"
  echo "-o|--out-path PATH        dir path where to export the final artifacts (DEFAULT=<SCRIPTDIR>/out)"
  echo "-t|--release-tag TAG      current release tag/version (DEFAULT=latest)"
  echo "-v|--verbose              enable verbose output (debugging infos)"
  echo
  [[ -z "${1}" ]] || exit ${1}
}
# -- /script usage


# -- args parsing
# reset params & set default settings
params=""
sys_name=""
# no params
[[ -n "${1}" ]] || NOARGS=true
# other params
while [[ -n "${1}" ]]
do
  case "${1}" in
    -h|--help|help)
      HELP=true
    ;;
    -o|--out-path)
      shift
      OUT_PATH="${1}"
    ;;
    -t|--release-tag)
      shift
      case "${1}" in
        master)
          RELEASE_TAG="latest"
          ;;
        VERSION-*|RELEASE-*)
          RELEASE_TAG="${1##*-}"
          ;;
        *)
          RELEASE_TAG="${1}"
          ;;
      esac
    ;;
    -v|--verbose)
      VERBOSE=true
    ;;
    ALP3X|alp3x|LTS18|lts18|RHEL7|rhel7|RHEL8|rhel8)
      sys_name="${1}"
    ;;
    *)
      params+="${1} "
    ;;
  esac
  shift
done
# -- /args parsing


# -- args checking
# print usage
if [[ "true" = "${HELP}" ]]
then
  USAGE 0
fi
# invalid params
if [[ -n "${params}" ]]
then
  USAGE
  ERROR "invalid parameter(s): ${params}"
fi
# invalid sys_name
if [[ -z "${sys_name}" ]]
then
  USAGE
  ERROR "missing parameter(s): SYS_NAME"
fi
# -- /args checking


# ENABLE ERROR HANDLING
# =====================

# exit on any error
set -e

# trap error events
report_errors() { echo -e "\033[31m[\033[33m${scriptName}\033[31m] ERROR occured on line [\033[33m${1}\033[31m]\033[0m"; }
trap 'report_errors ${LINENO}' ERR

# switch to workspace
[[ -n "${WORKSPACE}" ]] || WORKSPACE=${PWD}
cd ${WORKSPACE} || false
PRINT "starting build from WORKSPACE: ${PWD}"


# SETUP RUNTIME ENVIRONMENT
# =========================

# load global build settings first
global_environment_file=${scriptFullDir}/build.env
if [[ -f ${global_environment_file} ]]
then
  PRINT "importing global_environment_file: ${global_environment_file}"
  set -o allexport
  source ${global_environment_file}
  set +o allexport
fi


# START DOCKER BUILD FOR TARGET SYSTEM
# ====================================

printTitle "BUILDING OFFLINE PACKAGE FOR [${sys_name}] (RELEASE_TAG=${RELEASE_TAG})"

# check sys-specific build basepath
sys_build_basepath=${scriptFullDir}/sys/${sys_name}
if [[ -d ${sys_build_basepath} ]]
then
  DEBUG "using sys build settings from ${sys_build_basepath}"
else
  ERROR "invalid sys build path: ${sys_build_basepath}"
fi

# load sys-specific build settings
sys_environment_file=${sys_build_basepath}/build.env
if [[ -f ${sys_environment_file} ]]
then
  PRINT "importing sys_environment_file: ${sys_environment_file}"
  set -o allexport
  source ${sys_environment_file}
  set +o allexport
fi

# show debug infos
if [[ "true" = "${VERBOSE}" ]]
then
  printTitle "CURRENT ENVIRONMENT"
  printEnvs
fi


# start sys-specific docker build
# -- compute build image properties
img_name=$(tr "A-Z" "a-z" <<<"${sys_name}")
img_tag=${RELEASE_TAG}
# -- run docker build
time docker build --no-cache \
  --build-arg SYS_IMAGE="${SYS_IMAGE}" \
  --build-arg SYS_NAME="${sys_name}" \
  --tag ${img_name}:${img_tag} .


# extract release archive from build image
printTitle "EXPORT RELEASE ARCHIVE (RELEASE_TAG=${RELEASE_TAG})"
SRC_RELEASE_FILE="/release.tgz"
DST_RELEASE_FILE="${OUT_PATH}/release-${sys_name}-${RELEASE_TAG}.tgz"
TMP_CONTAINER=$(docker create ${img_name}:${img_tag})
PRINT "extracting release from temporary container: ${TMP_CONTAINER}"
[[ -d "${OUT_PATH}" ]] || mkdir -p ${OUT_PATH}
docker cp ${TMP_CONTAINER}:${SRC_RELEASE_FILE} ${DST_RELEASE_FILE}
docker rm ${TMP_CONTAINER}
PRINT "build release artifact:"
ls -lah ${DST_RELEASE_FILE}


# upload release archive to ftp server
if [[ -n "${FTP_UPLOAD_URL}" ]];then
printTitle "UPLOAD RELEASE ARCHIVE (RELEASE_TAG=${RELEASE_TAG})"
  PRINT "uploading release file [${DST_RELEASE_FILE}] ..."
  FTP_FILENAME=${DST_RELEASE_FILE##*/}
  curl -T "${DST_RELEASE_FILE}" "${FTP_UPLOAD_URL}/${FTP_FILENAME}"
fi


# move release archive to local release path
if [[ -n "${DST_RELEASE_PATH}" ]];then
printTitle "MOVING RELEASE ARCHIVE (RELEASE_TAG=${RELEASE_TAG})"
  [[ -d "${DST_RELEASE_PATH}" ]] || mkdir -p ${DST_RELEASE_PATH}
  PRINT "moving release file to [${DST_RELEASE_PATH}] ..."
  cp -v ${DST_RELEASE_FILE} ${DST_RELEASE_PATH}/${DST_RELEASE_FILE##*/}
  rm -f ${DST_RELEASE_FILE}
  PRINT "final release artifact:"
  ls -lah ${DST_RELEASE_PATH}/${DST_RELEASE_FILE##*/}
fi


# eof
