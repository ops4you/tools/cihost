#!/bin/sh
PURPOSE="correcting virtualenv script paths after copy/relocation"
VERSION=1

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables


#-- script helpers
print_usage() {
  EXIT_CODE=${1}
  echo "[i] SCRIPT INFOS: ${scriptName} v${VERSION} (${PURPOSE})"
  echo "[i] SCRIPT USAGE: ${scriptName}"
  echo
  exit ${EXIT_CODE}
}


# -- get the originating location of the virtualenv
ENV_DEFINITION=$(grep -r "VIRTUAL_ENV=" ${scriptFullDir}/bin/activate)
[ -z "${VIRTUAL_ENV}" ] && eval ${ENV_DEFINITION}
if [ -z "${VIRTUAL_ENV}" ]
then
  echo "[x] could not read VIRTUAL_ENV var from ENV_DEFINITION (${ENV_DEFINITION}), please set VIRTUAL_ENV=/old/venv/path manually!"
  exit 1
else
  OLD_PATH=$(echo "${VIRTUAL_ENV}" | sed "s#/\$##")
  echo "[>] old virtualenv location: ${OLD_PATH}"
fi


# -- rewrite paths in script headers
NEW_PATH=${scriptFullDir}
echo "[>] new virtualenv location: ${NEW_PATH}"
if [ "${OLD_PATH}" = "${NEW_PATH}" ]
then
  echo "[i] this venv does not require relocation (OLD_PATH=NEW_PATH)"
  exit 0
fi
SCRIPTS_TO_PROCESS=$(grep -rl "${OLD_PATH}" ${scriptFullDir}/bin)
echo
echo
echo "[?] searching for scripts with old path..."
for SCRIPT in ${SCRIPTS_TO_PROCESS}
do
  sed -i -e "s#${OLD_PATH}/*#${NEW_PATH}/#" ${SCRIPT} && echo "[+] script ${SCRIPT} relocated to ${NEW_PATH}"
done

unset VIRTUAL_ENV
echo "[i] virtualenv has been relocated"
echo "[i] run [source ${scriptFullDir}/bin/activate] to start using it"


# eof
