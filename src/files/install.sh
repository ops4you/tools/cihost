#!/usr/bin/env bash
PURPOSE="offline installation script"

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptBaseName=$(basename $scriptName)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables


# -- SCRIPT DEFAULTS OPTIONS
DST_PATH="/opt/services"
UPDATE="false"
# -- /SCRIPT DEFAULTS OPTIONS


# -- auto: common helper functions
isContainer() { grep -qE "/(docker|lxc)/" /proc/1/cgroup 2>/dev/null; }
printEnvs()   { local ENV_EXCEPTIONS="(^LS_|^XDG_|^SSH_|=;)"; env|grep -vE "${ENV_EXCEPTIONS}"|sort; }
printTitle()  { echo;echo; local msg="${*}"; linesno=$(( ${#msg} + 4 )); msglines=$(for i in `seq ${linesno}`;do echo -n "=";done);echo -e "${msglines}\n# ${msg} #\n${msglines}"; }
DEBUG() { local msg="$(date +%F_%T) [${scriptBaseName}] DEBUG ${*}"; [[ "true" = "${VERBOSE}" ]] && echo -e "${msg}" || true; }
PRINT() { local msg="$(date +%F_%T) [${scriptBaseName}] INFO  ${*}"; echo -e "${msg}"; }
ERROR() { local msg="$(date +%F_%T) [${scriptBaseName}] ERROR ${*}"; echo -e "${msg}"; exit ${RC:-1}; }
installOsPackages(){
  local OS_PACKAGES="${*}"
    if apk --version >/dev/null 2>/dev/null;then      PRINT "using apk package manager";  apk update && apk add              ${OS_PACKAGES};
  elif apt --version >/dev/null 2>/dev/null;then      PRINT "using apt package manager";  apt-get update -qq && apt install -y       ${OS_PACKAGES};
  elif dnf --version >/dev/null 2>/dev/null;then      PRINT "using dnf package manager";  dnf install -y       ${OS_PACKAGES};
  elif yum --version >/dev/null 2>/dev/null;then      PRINT "using yum package manager";  yum install -y       ${OS_PACKAGES};
  elif microdnf -h   >/dev/null 2>/dev/null;then      PRINT "using µdnf package manager"; microdnf install -y  ${OS_PACKAGES};
  else ERROR "no supported package manager found";fi
}
# -- /auto: common helper functions


# -- script usage
USAGE() {
  printTitle "USAGE"
  echo "PURPOSE: ${PURPOSE}"
  echo "SYNTAX:  ${scriptName} [OPTIONS]"
  printTitle "OPTIONS"
  echo "-h|--help                 print out this help"
  echo "-p|--dst-path PATH        destination base path for installation (DEFAULT=${DST_PATH})"
  echo "-u|--update               run an update installation only (skip newinstall tasks)"
  echo "-v|--verbose              enable verbose output (debugging infos)"
  echo
  [[ -z "${1}" ]] || exit ${1}
}
# -- /script usage


# -- args parsing
# reset params & set default settings
params=""
# no params
[[ -n "${1}" ]] || NOARGS=true
# other params
while [[ -n "${1}" ]]
do
  case "${1}" in
    -h|--help|help)
      HELP=true
    ;;
    -u|--update)
      UPDATE=true
    ;;
    -v|--verbose)
      VERBOSE=true
    ;;
    -p|--dst-path)
      shift
      DST_PATH="${1}"
    ;;
    *)
      params+="${1} "
    ;;
  esac
  shift
done
# -- /args parsing


# -- args checking
# print usage
if [[ "true" = "${HELP}" ]]
then
  USAGE 0
fi
# invalid params
if [[ -n "${params}" ]]
then
  DEBUG "pass through parameter(s): ${params}"
fi
# -- /args checking


# exit on errors
set -e
report_errors() { echo -e "\033[31m] ERROR occured on line [\033[33m${1}\033[31m]\033[0m"; }
trap 'report_errors ${LINENO}' ERR




# ------------------------#
#  SET RUNTIME OPTIONS    #
# ------------------------#

# ansible
ANSIBLE_SRC_PATH="${scriptFullDir}/env"
ANSIBLE_VAR_PATH="${scriptFullDir}/${scriptName%.*}.var"
ANSIBLE_YML_PATH="${scriptFullDir}/${scriptName%.*}.yml"
ANSIBLE_OPTIONS="-e DST_PATH=${DST_PATH} -e UPDATE=${UPDATE}"




# ------------------------#
#  START INSTALLATION     #
# ------------------------#

# init ansible virtualenv
cat <<tac

+--------------------------+
| INIT ANSIBLE ENVIRONMENT |
+--------------------------+

tac
ANSIBLE_DST_PATH="${DST_PATH}/env"
[[ -d "${ANSIBLE_DST_PATH}" ]] || mkdir -p ${ANSIBLE_DST_PATH}
if [[ "true" != "${UPDATE}" ]]
then
  PRINT "copying virtual environment first (${ANSIBLE_SRC_PATH} => ${ANSIBLE_DST_PATH})"
  cp --recursive ${ANSIBLE_SRC_PATH}/* ${ANSIBLE_DST_PATH}/
  PRINT "relocating virtual environment ${ANSIBLE_DST_PATH}"
  bash ${ANSIBLE_DST_PATH}/virtualenv.relocate.sh
fi
PRINT "initializing ansible environment ${ANSIBLE_DST_PATH}"
. ${ANSIBLE_DST_PATH}/bin/activate
ansible --version


# run ansible playbook
cat <<tac

+--------------------------+
| RUN ANSIBLE PLAYBOOKS    |
+--------------------------+

tac
if [[ ! -f ${ANSIBLE_YML_PATH} ]];then
  ERROR "install playbook [${ANSIBLE_YML_PATH}] not found, aborting..."
else
  ANSIBLE_CMD="ansible-playbook ${ANSIBLE_YML_PATH} ${ANSIBLE_OPTIONS} ${params}"
  PRINT "starting ansible playbook [${ANSIBLE_CMD}]"
  time ${ANSIBLE_CMD}
fi


exit 0
# EOF
